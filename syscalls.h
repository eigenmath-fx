#ifndef SYSCALLS_H
#define SYSCALLS_H

void GetFKeyIconPointer( int FKeyNo, unsigned char *pBitmap );
void DisplayFKeyIcons( void );
void DisplayFKeyIcon( int FKeyPos, unsigned char *pBitmap );
int Cursor_SetPosition(char column, char row);
int Cursor_SetFlashStyle(short int flashstyle);
void Cursor_SetFlashMode(long flashmode);
void Cursor_SetFlashOff(void);
void Cursor_SetFlashOn( char flash_style ); 
char Setup_GetEntry(unsigned int index);
void TestMode(void);

#endif //SYSCALLS_H
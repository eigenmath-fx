# Eigenmath
This program is a fork of the port of the Eigenmath symbolic calculus engine, initiated by the CnCalc community.
This program is intended to run on the Casio fx-9860GII / Graph *5 monochrome calculators.

## Building the program
The code is, as it, only buildable using a modified version of the S.D.K. provided by Casio, in order to support a certain 64 bits based number implementation.
I'm not sure if I'm allowed to publish such stuff publicly, but be sure to contact me if you need any help to build it.
A GCC based build process is actually thought about, but remains quite hard to set up, because of the relative mess of the actual code…

## Library used

The LePhenixNoir's [Memory library](http://www.planet-casio.com/Fr/programmes/programme2435-1-memory-LePhenixNoir-add-in.html)
The LePhenixNoir's [Tex rendering engine](http://www.planet-casio.com/Fr/programmes/programme2849-last-WebCalc-Lephenixnoir-b5.html) he uses on his WebCalc program.
